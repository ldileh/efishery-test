# eFishery test 
Name of creator this repository  : Fadillah Achmad Imam (Dileh)
This mini project is cloned from my (repository template android)[https://gitlab.com/ldileh/template-android]

## Installation
Before clone this repository, make sure you have android studio (latest version) installed on your machine. This project was created on version Android Studio Bumblebee | 2021.1.1 Patch 2

To do the installation, you can follow the steps bellow:

### Clone repository using VCS Android Studio
- open android studio 
- on first interface of android studio, click button "Get from VCS"
- fill field url with url repository (ex: [using ssh] git@gitlab.com:ldileh/template-android.git)
- select a folder to place your source code
- click button clone on bottom of popup, and wait until it's finished
- try run this project

### Clone repository with command line
- open command line
- select a folder to place your source code (ex : [using comand terminal on linux] cd /path/folder/project)
- do clone this repository (ex :git clone gitlab.com:ldileh/template-android.git project_name)
- wait until it's finished
- after finish clone the project, open project file using android studio
- try run this project
