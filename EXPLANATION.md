# Penjelasan pemilihan tampilan dan interaksi tampilan

## Tampilan utama
Konsep ui yang terapakan pada mini project ini didasarkan pengalaman dan base tampilan pada konsep 
tampilan material untuk android (link)[https://material.io/] 

## Navigasi
Navigasi pada mini project yang dibuat menggunakan tampilan seminimal mungkin tetapi tetap memperhatikan
pengalaman user saat mengguanakan aplikasi

## Filter, Sort, dan tambah data
Fitur filter dan sort pada aplikasi dapat diakses dengan menekan menu atau tombol pada toolbar halaman (berada pada 
pojok kanan aplikasi). Hanya tetapi untuk menambahkan item atau data baru anda dapat menekan tombol pada pojok kiri bawah

## List data
Fitur memunculkan data pada aplikasi ini menerapkan tampilan berupa list item kartu / card dari material design

Sekian, Terima kasih

