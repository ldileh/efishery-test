package com.efishery.core.config

import com.efishery.core.utils.PageMessageUtil

object BaseConfig {

    /**
     * default configuration showing message of page
     */
    val messageType = PageMessageUtil.Type.TOAST
}