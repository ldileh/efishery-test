package com.efishery.test.config

import com.efishery.test.BuildConfig

object GlobalConfig {
    // check if build app is debug or not
    const val IS_DEBUG = BuildConfig.BUILD_TYPE == "debug"

    // name of app
    private const val APP_NAME = "app"

    // shared preference
    const val SHARED_PREFERENCE_SESSION = "${APP_NAME}_sp_session"
    const val SHARED_PREFERENCE_TEMP_DATA = "${APP_NAME}_sp_temp_data"

    // name of database
    const val DB_NAME = "${APP_NAME}_db"
}