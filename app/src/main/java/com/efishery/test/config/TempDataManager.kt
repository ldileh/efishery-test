package com.efishery.test.config

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import com.efishery.test.domain.remote.model.AreaResponseModel
import com.efishery.test.domain.remote.model.ItemResponseModel
import com.efishery.test.domain.remote.model.SizeResponseModel
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class TempDataManager(private val context: Context) {

    companion object{
        private const val KEY_TEMP = "key_temp_"
        private const val KEY_LIST = "${KEY_TEMP}_DATA_LIST"
        private const val KEY_AREA = "${KEY_TEMP}_DATA_AREA"
        private const val KEY_SIZE = "${KEY_TEMP}_DATA_SIZE"
    }

    @SuppressLint("CommitPrefEdits")
    fun setList(items: List<ItemResponseModel>) {
        if (items.isNotEmpty()){
            val tempList = getList().toMutableList()
            val newItems = mutableListOf<ItemResponseModel>()
            items.forEach { item ->
                if (tempList.find { it.uuid == item.uuid } == null){
                    newItems.add(item)
                }
            }

            if (newItems.isNotEmpty())
                tempList.addAll(newItems)

            val resultJson = Gson().toJson(tempList)

            // set editor
            getSessionSharedPreference().edit().apply {
                putString(KEY_LIST, resultJson)
                apply()
            }
        }
    }

    @SuppressLint("CommitPrefEdits")
    fun setArea(items: List<AreaResponseModel>) {
        if (items.isNotEmpty()){
            val tempArea = getArea().toMutableList()
            val newItems = mutableListOf<AreaResponseModel>()
            items.forEach { item ->
                if (tempArea.find { it.province == item.province && it.city == item.city } == null){
                    newItems.add(item)
                }
            }

            if (newItems.isNotEmpty())
                tempArea.addAll(newItems)

            val resultJson = Gson().toJson(tempArea)

            // set editor
            getSessionSharedPreference().edit().apply {
                putString(KEY_AREA, resultJson)
                apply()
            }
        }
    }

    @SuppressLint("CommitPrefEdits")
    fun setSize(items: List<SizeResponseModel>) {
        if (items.isNotEmpty()){
            val tempSize = getSize().toMutableList()
            val newItems = mutableListOf<SizeResponseModel>()
            items.forEach { item ->
                if (tempSize.find { it.size == item.size } == null){
                    newItems.add(item)
                }
            }

            if (newItems.isNotEmpty())
                tempSize.addAll(newItems)

            val resultJson = Gson().toJson(tempSize)

            // set editor
            getSessionSharedPreference().edit().apply {
                putString(KEY_SIZE, resultJson)
                apply()
            }
        }
    }

    fun addList(item: ItemResponseModel): ItemResponseModel{
        val result = getList().toMutableList().apply {
            add(item)
        }

        val resultJson = Gson().toJson(result)

        getSessionSharedPreference().edit().apply {
            putString(KEY_LIST, resultJson)
            apply()
        }

        return item
    }

    fun getList(): List<ItemResponseModel> {
        val json = getSessionSharedPreference().getString(KEY_LIST, null)

        return try {
            Gson().fromJson(json, object : TypeToken<List<ItemResponseModel>>() {}.type)
        }catch (ignore: Exception){
            listOf()
        }
    }

    fun getArea(): List<AreaResponseModel> {
        val json = getSessionSharedPreference().getString(KEY_AREA, null)

        return try {
            Gson().fromJson(json, object : TypeToken<List<AreaResponseModel>>() {}.type)
        }catch (ignore: Exception){
            listOf()
        }
    }

    fun getSize(): List<SizeResponseModel> {
        val json = getSessionSharedPreference().getString(KEY_SIZE, null)

        return try {
            Gson().fromJson(json, object : TypeToken<List<SizeResponseModel>>() {}.type)
        }catch (ignore: Exception){
            listOf()
        }
    }

    private fun getSessionSharedPreference(): SharedPreferences =
        context.getSharedPreferences(GlobalConfig.SHARED_PREFERENCE_TEMP_DATA, Context.MODE_PRIVATE)
}