package com.efishery.test.domain.remote.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class AreaResponseModel(
    @SerializedName("city")
    @Expose
    val city: String? = null,
    @SerializedName("province")
    @Expose
    val province: String? = null
): Serializable