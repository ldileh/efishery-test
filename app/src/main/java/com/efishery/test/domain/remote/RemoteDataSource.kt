package com.efishery.test.domain.remote

import com.efishery.core.base.BaseService
import com.efishery.test.domain.remote.model.ItemRequestModel
import javax.inject.Inject

class RemoteDataSource @Inject constructor(private val service: RemoteService): BaseService() {

    private val limitItem = 10

    suspend fun getList(page: Int = 1) = getResult {
        val offset = (page - 1) * limitItem

        service.list(offset = offset)
    }

    suspend fun addItemList(data: ItemRequestModel) = getResult {
        service.addItemList(listOf(data))
    }

    suspend fun getOptionsAreas() = getResult { service.optionsAreas() }

    suspend fun getOptionsSizes() = getResult { service.optionsSizes() }
}