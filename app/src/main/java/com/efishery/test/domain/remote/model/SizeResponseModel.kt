package com.efishery.test.domain.remote.model

import com.google.gson.annotations.SerializedName
import com.google.gson.annotations.Expose
import java.io.Serializable

data class SizeResponseModel(
    @SerializedName("size")
    @Expose
    val size: String? = null
): Serializable