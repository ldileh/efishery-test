package com.efishery.test.domain.local.model

data class DefaultItemList(
    val id: Int = -1,
    val text: String = "",
    val extra: Any? = null
)