package com.efishery.test.domain.remote.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class ItemRequestModel(
    @SerializedName("area_kota")
    @Expose
    val areaKota: String? = null,
    @SerializedName("area_provinsi")
    @Expose
    val areaProvinsi: String? = null,
    @SerializedName("komoditas")
    @Expose
    val komoditas: String? = null,
    @SerializedName("price")
    @Expose
    val price: String? = null,
    @SerializedName("size")
    @Expose
    val size: String? = null,
    @SerializedName("tgl_parsed")
    @Expose
    val tglParsed: String? = null,
    @SerializedName("timestamp")
    @Expose
    val timestamp: String? = null,
    @SerializedName("uuid")
    @Expose
    val uuid: String? = null
)