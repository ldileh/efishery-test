package com.efishery.test.domain.remote

import com.efishery.test.domain.remote.model.AreaResponseModel
import com.efishery.test.domain.remote.model.ItemRequestModel
import com.efishery.test.domain.remote.model.ItemResponseModel
import com.efishery.test.domain.remote.model.SizeResponseModel
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface RemoteService {

    @GET("list")
    suspend fun list(
        @Query("limit") limit: Int = 10,
        @Query("offset") offset: Int = 0
    ): Response<List<ItemResponseModel>>

    @POST("list")
    suspend fun addItemList(@Body data: List<ItemRequestModel>): Response<Any>

    @GET("option_area")
    suspend fun optionsAreas(): Response<List<AreaResponseModel>>

    @GET("option_size")
    suspend fun optionsSizes(): Response<List<SizeResponseModel>>
}