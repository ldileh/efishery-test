package com.efishery.test.domain.local

import android.content.Context
import android.os.Bundle
import com.efishery.test.config.AppDatabase
import com.efishery.test.config.SessionManager
import com.efishery.test.config.TempDataManager
import com.efishery.test.domain.remote.model.AreaResponseModel
import com.efishery.test.domain.remote.model.ItemResponseModel
import com.efishery.test.domain.remote.model.SizeResponseModel
import com.efishery.test.ui.dialog.AddItemDialog
import javax.inject.Inject

@Suppress("unused")
class LocalDataSource @Inject constructor(private val context: Context) {

    private val database by lazy { AppDatabase.getAppDatabase(context) }
    private val sessionManager by lazy { SessionManager(context) }
    private val tempDataManager by lazy { TempDataManager(context) }

    fun getTokenSession() = sessionManager.getToken()

    fun setSession(data: Bundle) = sessionManager.setSession(data)

    fun clearSession(callback: () -> Unit) = sessionManager.clearSession(callback)

    fun saveList(items: List<ItemResponseModel>) {
        tempDataManager.setList(items)
    }

    fun saveArea(items: List<AreaResponseModel>) {
        tempDataManager.setArea(items)
    }

    fun saveSize(items: List<SizeResponseModel>) {
        tempDataManager.setSize(items)
    }

    fun getList() = tempDataManager.getList()

    fun getArea() = tempDataManager.getArea()

    fun getSize() = tempDataManager.getSize()

    fun saveItem(data: Bundle) = tempDataManager.addList(data.parseItem())

    private fun Bundle.parseItem(): ItemResponseModel {
        val komoditas = getString(AddItemDialog.FIELD_KOMODITAS) ?: ""
        val lokasi = getString(AddItemDialog.FIELD_LOKASI) ?: ""
        val harga = getString(AddItemDialog.FIELD_HARGA) ?: ""
        val ukuran = getString(AddItemDialog.FIELD_UKURAN) ?: ""

        var provinsi = ""
        var kota = ""
        if(lokasi.isNotEmpty()){
            val container = lokasi.split("-")

            provinsi = container[0].trimEnd()
            kota = container[1].trim()
        }

        return ItemResponseModel(
            uuid = getRandomString(),
            areaProvinsi = provinsi,
            areaKota = kota,
            komoditas = komoditas,
            price = harga,
            size = ukuran,
        )
    }

    private fun getRandomString(length: Int = 30) : String {
        val allowedChars = ('A'..'Z') + ('a'..'z') + ('0'..'9')
        return (1..length)
            .map { allowedChars.random() }
            .joinToString("")
    }
}