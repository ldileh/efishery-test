package com.efishery.test.domain.usecase

import android.os.Bundle
import com.efishery.core.base.BaseUseCase
import com.efishery.core.utils.Resource
import com.efishery.test.domain.local.LocalDataSource
import com.efishery.test.domain.remote.RemoteDataSource
import com.efishery.test.domain.remote.model.AreaResponseModel
import com.efishery.test.domain.remote.model.ItemRequestModel
import com.efishery.test.domain.remote.model.ItemResponseModel
import com.efishery.test.domain.remote.model.SizeResponseModel
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

@Suppress("unused")
class MainUseCase @Inject constructor(
    private val remoteData: RemoteDataSource,
    private val localData: LocalDataSource
): BaseUseCase() {

    override fun onTokenExpired() {
        clearSession()
    }

    private fun List<ItemResponseModel>.saveList(){
        localData.saveList(this)
    }

    private fun List<AreaResponseModel>.saveArea(){
        localData.saveArea(this)
    }

    private fun List<SizeResponseModel>.saveSize(){
        localData.saveSize(this)
    }

    private fun getListFromLocal() = Resource.Success(localData.getList())

    private fun getAreaFromLocal() = Resource.Success(localData.getArea())

    private fun getSizeFromLocal() = Resource.Success(localData.getSize())

    suspend fun callList(page: Int = 1) = handleResponse {
        // do manipulate response
        remoteData.getList(page).let { resource ->
            if(resource.error == null){
                val remote = resource.apply {
                    // set data items only uuid not null or empty
                    // and remove duplicate item by uuid
                    data = data?.filter { !it.uuid.isNullOrEmpty() }

                    // if data is not empty, save data to local
                    if (!data.isNullOrEmpty()){
                        data?.saveList()
                    }
                }


                // compare between remote data and local data,
                // if contains difference, show to ui
                val newItems = mutableListOf<ItemResponseModel>()
                val local = getListFromLocal().data
                local?.forEach { itemLocal ->
                    if(remote.data?.contains(itemLocal) == false){
                        newItems.add(itemLocal)
                    }
                }

                // add new items
                val data = (remote.data?.toMutableList() ?: mutableListOf())    .apply {
                    if(newItems.isNotEmpty())
                        addAll(newItems)
                }.distinctBy { it.uuid }

                // result result data
                remote.apply {
                    this.data = data
                }
            }else{
                if (resource.error is Resource.Failure.ErrorHolder.NetworkConnection){
                    // when internet is not connected
                    // return resource data from local,
                    // that previously was saved on local
                    getListFromLocal()
                }else
                    resource
            }
        }
    }

    suspend fun getOptionsAreas() = handleResponse {
        remoteData.getOptionsAreas().let { resource ->
            if(resource.error == null){
                resource.apply {
                    data = data?.filter { !it.city.isNullOrEmpty() && !it.province.isNullOrEmpty() }

                    // if data is not empty, save data to local
                    if (!data.isNullOrEmpty()){
                        data?.saveArea()
                    }
                }
            }else{
                if (resource.error is Resource.Failure.ErrorHolder.NetworkConnection){
                    // when internet is not connected
                    // return resource data from local,
                    // that previously was saved on local
                    getAreaFromLocal()
                }else
                    resource
            }
        }
    }

    suspend fun getOptionsSizes() = handleResponse {
        remoteData.getOptionsSizes().let { resource ->
            if(resource.error == null){
                resource.apply {
                    data = data?.filter { !it.size.isNullOrEmpty() }

                    // if data is not empty, save data to local
                    if (!data.isNullOrEmpty()){
                        data?.saveSize()
                    }
                }
            }else{
                if (resource.error is Resource.Failure.ErrorHolder.NetworkConnection){
                    // when internet is not connected
                    // return resource data from local,
                    // that previously was saved on local
                    getSizeFromLocal()
                }else
                    resource
            }
        }
    }

    fun getTokenSession() = localData.getTokenSession()

    fun setSession(data: Bundle) = localData.setSession(data)

    suspend fun addItem(data: Bundle) = localData.saveItem(data).also { item ->
        remoteData.addItemList(item.parse())
    }

    @Suppress("WeakerAccess")
    fun clearSession() = localData.clearSession{
        // put action after clear session
    }

    private fun ItemResponseModel.parse() = ItemRequestModel(
        uuid = uuid,
        areaKota = areaKota,
        areaProvinsi = areaProvinsi,
        komoditas = komoditas,
        price = price,
        size = size,
        tglParsed = getCurrentDateTime().parseToString("dd/M/yyyy hh:mm:ss"),
        timestamp = System.currentTimeMillis().toString()
    )

    private fun Date.parseToString(format: String, locale: Locale = Locale.getDefault()): String {
        val formatter = SimpleDateFormat(format, locale)
        return formatter.format(this)
    }

    private fun getCurrentDateTime(): Date {
        return Calendar.getInstance().time
    }
}