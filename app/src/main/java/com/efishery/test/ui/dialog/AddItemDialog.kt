package com.efishery.test.ui.dialog

import android.content.Context
import android.os.Bundle
import android.widget.AutoCompleteTextView
import com.efishery.test.R
import com.efishery.test.databinding.DialogAddItemBinding
import com.efishery.test.domain.local.model.DefaultItemList
import com.efishery.test.domain.remote.model.AreaResponseModel
import com.efishery.test.domain.remote.model.SizeResponseModel
import com.efishery.test.ui.adapter.AutoCompleteTextViewAdapter
import com.google.android.material.bottomsheet.BottomSheetDialog

class AddItemDialog (
    context: Context,
    private val actionAdd: ActionAdd
): BottomSheetDialog(context) {

    private lateinit var binding: DialogAddItemBinding

    private val optionsLokasi = mutableListOf<DefaultItemList>()

    private var adapterLokasi: AutoCompleteTextViewAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DialogAddItemBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.initViews()
    }

    override fun onDetachedFromWindow() {
        binding.setEmptyFields()

        super.onDetachedFromWindow()
    }

    fun setItemsOptionLokasi(items: List<DefaultItemList>){
        this.optionsLokasi.apply {
            clear()
            addAll(items)
        }

        adapterLokasi?.notifyDataSetChanged()
    }

    private fun DialogAddItemBinding.initViews(){
        adapterLokasi = spLokasi.configureOption(optionsLokasi)

        btnSubmit.setOnClickListener {
            // call event submit
            actionAdd.onSubmitAdd(Bundle().apply {
                putString(FIELD_KOMODITAS, edtKomoditas.text.toString())
                putString(FIELD_LOKASI, spLokasi.text.toString())
                putString(FIELD_HARGA, edtHarga.text.toString())
                putString(FIELD_UKURAN, edtUkuran.text.toString())
            })

            // close dialog
            dismiss()
        }
    }

    private fun DialogAddItemBinding.setEmptyFields(){
        edtKomoditas.setText("")
        spLokasi.setText("")
        edtHarga.setText("")
        edtUkuran.setText("")
    }

    private fun generateAdapterOptions(items: List<DefaultItemList>) =
        AutoCompleteTextViewAdapter(context, R.layout.item_option, R.id.tv_text, items)

    private fun AutoCompleteTextView.configureOption(items: List<DefaultItemList>) =
        generateAdapterOptions(items).let { adapter ->
            setAdapter(adapter)
            adapter
        }

    interface ActionAdd{

        fun onSubmitAdd(data: Bundle)
    }

    companion object{

        const val FIELD_KOMODITAS = "field_komoditas"
        const val FIELD_LOKASI = "field_lokasi"
        const val FIELD_HARGA = "field_harga"
        const val FIELD_UKURAN = "field_ukuran"

        fun newInstance(context: Context, actionAdd: ActionAdd) = AddItemDialog(context, actionAdd)
    }
}