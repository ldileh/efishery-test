package com.efishery.test.ui.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.annotation.IdRes
import androidx.annotation.LayoutRes
import com.efishery.test.R
import com.efishery.test.domain.local.model.DefaultItemList

class AutoCompleteTextViewAdapter(
    context: Context,
    @LayoutRes private val resource: Int,
    @IdRes private val textViewResourceId: Int,
    private val items: List<DefaultItemList>
): ArrayAdapter<String>(context, resource, textViewResourceId) {

    @SuppressLint("ViewHolder")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View = LayoutInflater
        .from(parent.context)
        .inflate(resource, parent, false)
        .apply {
            findViewById<TextView>(R.id.tv_text).apply {
                text = getItem(position)
            }
        }

    override fun getItem(position: Int): String = items[position].text

    override fun getItemId(position: Int): Long = items[position].id.toLong()

    override fun getCount(): Int = items.size

    fun getItemExtra(text: String): Any? = items.find { it.text == text }?.extra
}