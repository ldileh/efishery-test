package com.efishery.test.ui.view.main

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.recyclerview.widget.RecyclerView
import com.efishery.test.databinding.ItemMainBinding
import com.efishery.test.domain.remote.model.ItemResponseModel
import timber.log.Timber
import java.lang.Exception
import java.text.NumberFormat
import java.util.*

class MainAdapter: RecyclerView.Adapter<MainAdapter.ViewHolder>() {

    private val mItems = mutableListOf<ItemResponseModel>()

    inner class ViewHolder(private val itemViewBinding: ItemMainBinding): RecyclerView.ViewHolder(itemViewBinding.root){

        fun bind(item: ItemResponseModel, position: Int) {
            itemViewBinding.apply {
                configureItemView(position)

                cfUuid.setValue(item.uuid ?: "-")
                cfKomoditas.setValue(item.komoditas ?: "-")
                cfLokasi.setValue("${item.areaProvinsi ?: "-"} - ${item.areaKota ?: "-"}")
                cfSize.setValue(item.size ?: "-")
                cfPrice.setValue(item.price.formatRupiah() ?: "-")
            }
        }

        private fun ItemMainBinding.configureItemView(position: Int){
            root.apply {
                layoutParams = LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
                ).apply {
                    when (position) {
                        0 -> setMargins(32.dp, 8.dp, 32.dp, 4.dp)
                        (itemCount - 1) -> setMargins(32.dp, 4.dp, 32.dp, 8.dp)
                        else -> setMargins(32.dp, 4.dp, 32.dp, 4.dp)
                    }
                }
            }
        }

        private val Int.dp: Int get() = (this / itemView.resources.displayMetrics.density).toInt()

        private fun String?.formatRupiah(): String? {
            val result = try {
                if (this == null) return "-"

                val localeID = Locale("in", "ID")
                val formatRupiah: NumberFormat = NumberFormat.getCurrencyInstance(localeID)
                formatRupiah.format(this.toDouble())
            } catch (e: Exception) {
                Timber.d(e)
                this ?: "-"
            }

            return result
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(ItemMainBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(mItems[position], position)
    }

    override fun getItemCount(): Int = mItems.size

    fun setItems(items: List<ItemResponseModel>, isReset: Boolean = false){
        if (isReset){
            mItems.apply {
                clearAllItems()

                addAll(items)
            }

            notifyItemRangeInserted(0, items.size)
        }else
            addItems(items)
    }

    fun addItems(items: List<ItemResponseModel>){
        // get size of list items
        val endPosition = mItems.size

        // add all items to container items
        mItems.apply {
            addAll(items)
        }

        // notify list adapter to update the view.
        // because its add from last size of items container, position start is from size
        // container items to new size of container items
        notifyItemRangeInserted(endPosition, mItems.size)
    }

    fun addItem(item: ItemResponseModel){
        mItems.add(0, item)

        notifyItemInserted(0)
    }

    fun clearItems(){
        mItems.apply {
            clearAllItems()
        }
    }

    fun lastItem(): ItemResponseModel? = if(mItems.isNotEmpty()) mItems.last() else null

    fun getItems() = mItems

    private fun MutableList<ItemResponseModel>.clearAllItems(){
        // get size of list items
        val endPosition = size

        // clear list items
        clear()

        // notify list adapter, all items is removed.
        // because its removed all items, position start is from first item to last item
        notifyItemRangeRemoved(0, endPosition)
    }
}