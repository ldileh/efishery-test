package com.efishery.test.ui.view.main

import android.os.Bundle
import androidx.fragment.app.viewModels
import com.efishery.core.base.BaseFragmentVM
import com.efishery.test.databinding.FragmentMainBinding
import com.efishery.test.ui.viewmodel.MainViewModel

@Suppress("unused")
class MainFragment: BaseFragmentVM<FragmentMainBinding, MainViewModel>(FragmentMainBinding::bind) {

    override val viewModel: MainViewModel by viewModels()

    override fun FragmentMainBinding.onViewCreated(savedInstanceState: Bundle?) {

    }

    override fun MainViewModel.vmObserver() = apply {

    }
}