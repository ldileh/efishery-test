package com.efishery.test.ui.component

import android.content.Context
import android.content.res.TypedArray
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.StringRes
import com.efishery.test.R

class ContainerField(
    context: Context,
    attrs: AttributeSet
): LinearLayout(context, attrs) {

    private val tvLabel: TextView
    private val tvValue: TextView

    init {
        val view = inflate(context, R.layout.view_container_field, this)

        tvLabel = view.findViewById(R.id.vcf_label)
        tvValue = view.findViewById(R.id.vcf_value)

        val typedArray: TypedArray = context.obtainStyledAttributes(attrs, R.styleable.ContainerField, 0, 0)

        tvLabel.text = typedArray.getString(R.styleable.ContainerField_cf_label) ?: "-"
        tvValue.text = typedArray.getString(R.styleable.ContainerField_cf_value) ?: "-"

        typedArray.recycle()
    }

    fun setLabel(text: String){
        tvLabel.text = text
    }

    fun setLabel(@StringRes textResource: Int){
        tvLabel.setText(textResource)
    }

    fun setValue(text: String){
        tvValue.text = text
    }

    fun setValue(@StringRes textResource: Int){
        tvValue.setText(textResource)
    }
}