package com.efishery.test.ui.view.main

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.activity.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.efishery.core.base.BaseActivityVM
import com.efishery.core.utils.PageMessageUtil
import com.efishery.test.R
import com.efishery.test.databinding.ActivityMainBinding
import com.efishery.test.databinding.ContentActivityMainBinding
import com.efishery.test.domain.local.model.DefaultItemList
import com.efishery.test.domain.remote.model.ItemResponseModel
import com.efishery.test.ui.dialog.AddItemDialog
import com.efishery.test.ui.dialog.FilterItemDialog
import com.efishery.test.ui.dialog.OrderItemDialog
import com.efishery.test.ui.viewmodel.MainViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : BaseActivityVM<ActivityMainBinding, MainViewModel>(ActivityMainBinding::inflate),
    FilterItemDialog.ActionFilter, OrderItemDialog.ActionOrder, AddItemDialog.ActionAdd {

    private var page = 1

    private var isReachBottom = false
    private var pastVisibleItems = 0
    private var visibleItemCount = 0
    private var totalItemCount = 0

    private var isActionFilter = false
    private var isActionOrder = false

    private val dialogFilter: FilterItemDialog by lazy {
        FilterItemDialog.newInstance(this@MainActivity, this@MainActivity)
    }

    private val dialogOrder: OrderItemDialog by lazy {
        OrderItemDialog.newInstance(this@MainActivity, this@MainActivity)
    }

    private val dialogAdd: AddItemDialog by lazy {
        AddItemDialog.newInstance(this@MainActivity, this@MainActivity)
    }

    override val viewModel: MainViewModel by viewModels()

    override var messageType: PageMessageUtil.Type = PageMessageUtil.Type.SNACK_BAR

    override fun toolbarId(): Int = R.id.toolbar

    override var isDisplayHomeAsUp: Boolean = false

    override fun ActivityMainBinding.onViewCreated(savedInstanceState: Bundle?) {
        initViews()

        fetchAllData()

        /*
            events
         */

        viewBody.viewRefresh.setOnRefreshListener { fetchList(isReset = true) }

        viewBody.list.addOnScrollListener(object : RecyclerView.OnScrollListener() {

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (dy > 0) { //check for scroll down
                    visibleItemCount = getListLayoutManager().childCount
                    totalItemCount = getListLayoutManager().itemCount
                    pastVisibleItems = getListLayoutManager().findFirstVisibleItemPosition()

                    if (viewModel.eventList.value == false && !isReachBottom && !isActionFilter && !isActionOrder) {
                        if (visibleItemCount + pastVisibleItems >= totalItemCount) {
                            fetchList(page.inc())
                        }
                    }
                }
            }
        })

        fabAdd.setOnClickListener {
            showDialogAdd(true)
        }

        viewBody.btnReset.setOnClickListener {
            when{
                isActionFilter -> viewModel.resetFilter()
                isActionOrder -> viewModel.resetOrder()
            }
        }
    }

    override fun MainViewModel.observeViewModel() = apply {
        dataList.observe(this@MainActivity) { items ->
            getListAdapter().setItems(items, page == 1)

            // show empty view if items is empty
            binding.viewBody.showEmptyView(getListAdapter().itemCount == 0, getString(R.string.text_data_is_empty))
        }

        eventList.observe(this@MainActivity) { binding.viewBody.viewRefresh.isRefreshing = it }

        isReachBottom.observe(this@MainActivity) { this@MainActivity.isReachBottom = it }

        dataOptionArea.observe(this@MainActivity) { items -> setDialogItemsArea(items) }

        dataOptionSize.observe(this@MainActivity) { items -> setDialogItemsSize(items) }

        pageNumberChange.observe(this@MainActivity) { newPage -> page = newPage }

        isResetList.observe(this@MainActivity) { isReset -> if(isReset) getListAdapter().clearItems() }

        dataListFilter.observe(this@MainActivity) { resultFilter ->
            setActionFilter(resultFilter, true)

            // show empty view if items is empty
            binding.viewBody.showEmptyView(getListAdapter().itemCount == 0, getString(R.string.text_data_is_not_found))
        }

        resetListFilter.observe(this@MainActivity) { originalItems ->
            if(originalItems.isNotEmpty())
                setActionFilter(originalItems, false)

            // show empty view if items is empty
            binding.viewBody.showEmptyView(getListAdapter().itemCount == 0)
        }

        dataListOrder.observe(this@MainActivity) { resultOrder ->
            setActionOrder(resultOrder, true)

            // show empty view if items is empty
            binding.viewBody.showEmptyView(getListAdapter().itemCount == 0, getString(R.string.text_data_is_empty))
        }

        resetListOrder.observe(this@MainActivity) { originalItems ->
            if(originalItems.isNotEmpty())
                setActionOrder(originalItems, false)

            // show empty view if items is empty
            binding.viewBody.showEmptyView(getListAdapter().itemCount == 0)
        }

        resultAddItem.observe(this@MainActivity) { item -> getListAdapter().addItem(item) }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            android.R.id.home -> onBackPressed()

            R.id.filter -> {
                if (!binding.viewBody.viewRefresh.isRefreshing)
                    showDialogFilter(true)
            }

            R.id.order -> {
                if (!binding.viewBody.viewRefresh.isRefreshing)
                    showDialogOrder(true)
            }
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onSubmitFilter(data: Bundle) {
        viewModel.filterList(getListAdapter().getItems(), data)
    }

    override fun onResetFilter() {
        viewModel.resetFilter()
    }

    override fun onSubmitOrder(data: Bundle) {
        viewModel.orderList(getListAdapter().getItems(), data)
    }

    override fun onResetOrder() {
        viewModel.resetOrder()
    }

    override fun onSubmitAdd(data: Bundle) {
        viewModel.addItemList(data)
    }

    private fun ActivityMainBinding.initViews(){
        viewBody.configureList()
    }

    private fun ContentActivityMainBinding.configureList(){
        list.adapter = MainAdapter()
    }

    private fun ContentActivityMainBinding.showEmptyView(isShow: Boolean, message: String? = null){
        if (isShow){
            list.visibility = View.GONE
            viewContainerEmpty.visibility = View.VISIBLE
            viewEmpty.playAnimation()

            message?.let { msg -> tvInformation.text = msg }
        }else{
            list.visibility = View.VISIBLE
            viewContainerEmpty.visibility = View.GONE
            viewEmpty.pauseAnimation()

            tvInformation.text = ""
        }
    }

    private fun getListAdapter() = binding.viewBody.list.adapter as MainAdapter

    private fun getListLayoutManager() = binding.viewBody.list.layoutManager as LinearLayoutManager

    private fun fetchList(page: Int = 1, isReset: Boolean = false){
        // configure some attributes pagination
        if(isReset){
            this.page = 1
            this.isReachBottom = false
        }else{
            this.page = page
        }

        // do action fetch data
        viewModel.getList(
            page = page,
            lastItem = if(page > 1) getListAdapter().lastItem() else null
        )
    }

    private fun fetchAllData(){
        fetchList()

        viewModel.getDataFilterDialog()
    }

    @Suppress("SameParameterValue")
    private fun showDialogFilter(isShow: Boolean){
        dialogFilter.apply {
            if (isShow){
                if (!isShowing)
                    show()
            }else{
                if (isShowing)
                    dismiss()
            }
        }
    }

    @Suppress("SameParameterValue")
    private fun showDialogOrder(isShow: Boolean){
        dialogOrder.apply {
            if (isShow){
                if (!isShowing)
                    show()
            }else{
                if (isShowing)
                    dismiss()
            }
        }
    }

    @Suppress("SameParameterValue")
    private fun showDialogAdd(isShow: Boolean){
        dialogAdd.apply {
            if (isShow){
                if (!isShowing)
                    show()
            }else{
                if (isShowing)
                    dismiss()
            }
        }
    }

    private fun setDialogItemsArea(items: List<DefaultItemList>){
        dialogFilter.setItemsOptionArea(items)
        dialogAdd.setItemsOptionLokasi(items)
    }

    private fun setDialogItemsSize(items: List<DefaultItemList>){
        dialogFilter.setItemsOptionSize(items)
    }

    private fun setActionFilter(items: List<ItemResponseModel>, isFilter: Boolean){
        // set items of list
        getListAdapter().setItems(items, true)

        // enable action pull refresh
        binding.viewBody.viewRefresh.isEnabled = !isFilter

        // set indicator filter
        isActionFilter = isFilter
    }

    private fun setActionOrder(items: List<ItemResponseModel>, isOrder: Boolean){
        // set items of list
        getListAdapter().setItems(items, true)

        // enable action pull refresh
        binding.viewBody.viewRefresh.isEnabled = !isOrder

        // set indicator filter
        isActionOrder = isOrder
    }
}