package com.efishery.test.ui.dialog

import android.content.Context
import android.os.Bundle
import android.widget.AutoCompleteTextView
import com.efishery.test.R
import com.efishery.test.databinding.DialogFilterItemBinding
import com.efishery.test.domain.local.model.DefaultItemList
import com.efishery.test.domain.remote.model.AreaResponseModel
import com.efishery.test.domain.remote.model.SizeResponseModel
import com.efishery.test.ui.adapter.AutoCompleteTextViewAdapter
import com.google.android.material.bottomsheet.BottomSheetDialog

class FilterItemDialog(
    context: Context,
    private val actionFilter: ActionFilter
): BottomSheetDialog(context) {

    private lateinit var binding: DialogFilterItemBinding

    private val optionsArea = mutableListOf<DefaultItemList>()
    private val optionsSize = mutableListOf<DefaultItemList>()

    private var adapterArea: AutoCompleteTextViewAdapter? = null
    private var adapterSize: AutoCompleteTextViewAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DialogFilterItemBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.initViews()
    }

    fun setItemsOptionArea(items: List<DefaultItemList>){
        this.optionsArea.apply {
            clear()
            addAll(items)
        }

        adapterArea?.notifyDataSetChanged()
    }

    fun setItemsOptionSize(items: List<DefaultItemList>){
        this.optionsSize.apply {
            clear()
            addAll(items)
        }

        adapterSize?.notifyDataSetChanged()
    }

    private fun DialogFilterItemBinding.initViews(){
        adapterArea = spArea.configureOption(optionsArea)
        adapterSize = spSize.configureOption(optionsSize)

        btnSubmit.setOnClickListener {
            // call event submit
            actionFilter.onSubmitFilter(Bundle().apply {
                putSerializable(FIELD_AREA, adapterArea?.getItemExtra(spArea.text.toString()) as AreaResponseModel?)
                putSerializable(FIELD_SIZE, adapterSize?.getItemExtra(spSize.text.toString()) as SizeResponseModel?)
                putSerializable(FIELD_SEARCH, edtSearch.text.toString())
            })

            // close dialog
            dismiss()
        }

        btnReset.setOnClickListener {
            // set empty fields on dialog filter
            setEmptyFields()

            // call event reset
            actionFilter.onResetFilter()

            // close dialog
            dismiss()
        }
    }

    private fun DialogFilterItemBinding.setEmptyFields(){
        spArea.setText("")
        spSize.setText("")
    }

    private fun generateAdapterOptions(items: List<DefaultItemList>) =
        AutoCompleteTextViewAdapter(context, R.layout.item_option, R.id.tv_text, items)

    private fun AutoCompleteTextView.configureOption(items: List<DefaultItemList>) =
        generateAdapterOptions(items).let { adapter ->
            setAdapter(adapter)
            adapter
        }

    interface ActionFilter{

        fun onSubmitFilter(data: Bundle)
        fun onResetFilter()
    }

    companion object{

        const val FIELD_AREA = "field_area"
        const val FIELD_SIZE = "field_size"
        const val FIELD_SEARCH = "field_search"

        fun newInstance(context: Context, actionFilter: ActionFilter) = FilterItemDialog(context, actionFilter)
    }
}