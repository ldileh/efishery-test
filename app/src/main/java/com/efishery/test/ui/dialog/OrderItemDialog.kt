package com.efishery.test.ui.dialog

import android.content.Context
import android.os.Bundle
import android.widget.AutoCompleteTextView
import com.efishery.test.R
import com.efishery.test.databinding.DialogOrderItemBinding
import com.efishery.test.domain.local.model.DefaultItemList
import com.efishery.test.ui.adapter.AutoCompleteTextViewAdapter
import com.google.android.material.bottomsheet.BottomSheetDialog

class OrderItemDialog(
    context: Context,
    private val actionOrder: ActionOrder
): BottomSheetDialog(context) {

    private lateinit var binding: DialogOrderItemBinding

    private val options: MutableList<DefaultItemList> by lazy {
        mutableListOf(
            DefaultItemList(1, context.getString(R.string.label_komoditas), ORDER_KOMODITAS),
            DefaultItemList(2, context.getString(R.string.label_harga), ORDER_PRICE),
            DefaultItemList(3, context.getString(R.string.label_ukuran), ORDER_UKURAN),
        )
    }

    private var adapterOption: AutoCompleteTextViewAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DialogOrderItemBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.initViews()
    }

    private fun DialogOrderItemBinding.initViews(){
        adapterOption = spOrder.configureOption(options)

        btnAsc.setOnClickListener {
            // call event submit
            actionOrder.onSubmitOrder(generateResultOrder(
                option = adapterOption?.getItemExtra(spOrder.text.toString()) as String?,
                order = ORDER_ACTION_ASC
            ))

            // close dialog
            dismiss()
        }

        btnDesc.setOnClickListener {
            // call event submit
            actionOrder.onSubmitOrder(generateResultOrder(
                option = adapterOption?.getItemExtra(spOrder.text.toString()) as String?,
                order = ORDER_ACTION_DESC
            ))

            // close dialog
            dismiss()
        }

        btnReset.setOnClickListener {
            // set empty fields on dialog filter
            setEmptyFields()

            // call event reset
            actionOrder.onResetOrder()

            // close dialog
            dismiss()
        }
    }

    private fun generateResultOrder(option: String?, order: String): Bundle = Bundle().apply {
        putSerializable(FIELD_OPTION, option)
        putSerializable(ORDER_ACTION, order)
    }

    private fun generateAdapterOptions(items: List<DefaultItemList>) =
        AutoCompleteTextViewAdapter(context, R.layout.item_option, R.id.tv_text, items)

    private fun AutoCompleteTextView.configureOption(items: List<DefaultItemList>) =
        generateAdapterOptions(items).let { adapter ->
            setAdapter(adapter)
            adapter
        }

    private fun DialogOrderItemBinding.setEmptyFields(){
        spOrder.setText("")
    }

    interface ActionOrder{

        fun onSubmitOrder(data: Bundle)
        fun onResetOrder()
    }

    companion object{
        const val FIELD_OPTION = "field_option"
        const val ORDER_ACTION = "order_action"

        const val ORDER_KOMODITAS = "order_komoditas"
        const val ORDER_PRICE = "order_price"
        const val ORDER_UKURAN = "order_ukuran"

        const val ORDER_ACTION_ASC = "order_action_asc"
        const val ORDER_ACTION_DESC = "order_action_desc"

        fun newInstance(context: Context, actionFilter: ActionOrder) = OrderItemDialog(context, actionFilter)
    }
}