package com.efishery.test.ui.viewmodel

import android.os.Bundle
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.efishery.core.base.BaseViewModel
import com.efishery.core.utils.Resource
import com.efishery.test.domain.local.model.DefaultItemList
import com.efishery.test.domain.remote.model.AreaResponseModel
import com.efishery.test.domain.remote.model.ItemResponseModel
import com.efishery.test.domain.remote.model.SizeResponseModel
import com.efishery.test.domain.usecase.MainUseCase
import com.efishery.test.ui.dialog.FilterItemDialog
import com.efishery.test.ui.dialog.OrderItemDialog
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val useCase: MainUseCase
) : BaseViewModel() {

    private val _eventList = MutableLiveData<Boolean>()
    private val _dataList = MutableLiveData<List<ItemResponseModel>>()
    private val _dataOptionArea = MutableLiveData<List<DefaultItemList>>()
    private val _dataOptionSize = MutableLiveData<List<DefaultItemList>>()
    private val _isReachBottom = MutableLiveData<Boolean>()
    private val _pageNumberChange = MutableLiveData<Int>()
    private val _isResetList = MutableLiveData<Boolean>()
    private val _dataListFilter = MutableLiveData<List<ItemResponseModel>>()
    private val _resetListFilter = MutableLiveData<List<ItemResponseModel>>()
    private val _dataListOrder = MutableLiveData<List<ItemResponseModel>>()
    private val _resetListOrder = MutableLiveData<List<ItemResponseModel>>()
    private val _resultAddItem = MutableLiveData<ItemResponseModel>()

    val eventList: LiveData<Boolean> = _eventList
    val dataList: LiveData<List<ItemResponseModel>> by lazy { _dataList }
    val dataOptionArea: LiveData<List<DefaultItemList>> by lazy { _dataOptionArea }
    val dataOptionSize: LiveData<List<DefaultItemList>> by lazy { _dataOptionSize }
    val isReachBottom: LiveData<Boolean> = _isReachBottom
    val pageNumberChange: LiveData<Int> = _pageNumberChange
    val isResetList: LiveData<Boolean> = _isResetList
    val dataListFilter: LiveData<List<ItemResponseModel>> = _dataListFilter
    val resetListFilter: LiveData<List<ItemResponseModel>> = _resetListFilter
    val dataListOrder: LiveData<List<ItemResponseModel>> = _dataListOrder
    val resetListOrder: LiveData<List<ItemResponseModel>> = _resetListOrder
    val resultAddItem: LiveData<ItemResponseModel> = _resultAddItem

    private var retryListNumber = 0
    private val maxRetryGetList = 9
    private val tempItemsList = mutableListOf<ItemResponseModel>()

    private val tempListFilter = mutableListOf<ItemResponseModel>()

    private val tempListOrder = mutableListOf<ItemResponseModel>()

    fun getList(page: Int, lastItem: ItemResponseModel? = null)  {
        launch {
            _eventList.postValue(true)

            useCase.callList(page).getResultCase { result ->
                _eventList.postValue(false)

                when(result){
                    is Resource.Success -> {
                        val items = result.data?.toMutableList() ?: mutableListOf()

                        if (items.isEmpty() && retryListNumber <= maxRetryGetList){
                            // do action re-request
                            // fetch data type 1

                            // do reset list item while response
                            // got empty on page 1
                            if(page == 1){
                                _isResetList.postValue(true)
                            }

                            // increase retry number
                            retryListNumber++

                            // increase page number and
                            // send to ui for new page number
                            val newPage = page.inc()
                            _pageNumberChange.postValue(newPage)

                            // re-request fetch data
                            getList(newPage, lastItem)
                            return@getResultCase
                        }

                        if(retryListNumber > 0){
                            if(items.size < 10 && tempItemsList.size < 10){
                                // do action re-request
                                // fetch data type 2

                                // store result to temp data
                                tempItemsList.addAll(items)

                                // increase retry number
                                retryListNumber++

                                // increase page number and
                                // send to ui for new page number
                                val newPage = page.inc()
                                _pageNumberChange.postValue(newPage)

                                // re-request data
                                getList(newPage, lastItem)
                                return@getResultCase
                            }

                            // apply result collection temp data as result items
                            items.apply {
                                clear()
                                addAll(tempItemsList)
                            }

                            // clear temp data items and
                            // reset retry number of fetch data
                            tempItemsList.clear()
                            retryListNumber = 0
                        }

                        // check for action endless scroll
                        if(lastItem != null){
                            val check = items.find { !it.uuid.isNullOrEmpty() && it.uuid == lastItem.uuid }
                            _isReachBottom.postValue(check != null)
                        }

                        // post result to ui
                        _dataList.postValue(items.distinctBy { it.uuid })
                    }

                    is Resource.Failure -> {
                        // do default action in here.
                        // just remove this method if didn't need
                        onError(result.error)

                        /*
                        do some action failure in here.
                         */
                        if(result.error is Resource.Failure.ErrorHolder.NetworkConnection){
                            // assume is pagination is reach the bottom of page
                            _isReachBottom.postValue(true)
                        }
                    }
                }
            }
        }
    }

    fun getDataFilterDialog() = launch{
        useCase.apply {
            getOptionsAreas().getResultCase { result ->
                when(result){
                    is Resource.Success -> {
                        val data = result.data?.mapIndexed { index, item ->
                            DefaultItemList(id = index, text = "${item.province} - ${item.city}", extra = item)
                        } ?: listOf()

                        _dataOptionArea.postValue(data)
                    }

                    is Resource.Failure -> { }
                }
            }

            getOptionsSizes().getResultCase { result ->
                when(result){
                    is Resource.Success -> {
                        val data = result.data?.mapIndexed { index, item ->
                            DefaultItemList(id = index, text = item.size ?: "-", extra = item)
                        } ?: listOf()

                        _dataOptionSize.postValue(data)
                    }

                    is Resource.Failure -> { }
                }
            }
        }
    }

    fun filterList(items: List<ItemResponseModel>, filter: Bundle) = launch {
        val filterArea = filter.getSerializable(FilterItemDialog.FIELD_AREA) as AreaResponseModel?
        val filterSize = filter.getSerializable(FilterItemDialog.FIELD_SIZE) as SizeResponseModel?
        val filterQuery = filter.getSerializable(FilterItemDialog.FIELD_SEARCH) as String?

        // set temp data items
        if(tempListFilter.isEmpty()){
            tempListFilter.addAll(items)
        }

        // do filtering item
        var result: List<ItemResponseModel> = tempListFilter

        if(filterArea != null) {
            result = result.filter {
                (it.areaProvinsi ?: "")
                    .trimEnd()
                    .lowercase()
                    .equals(filterArea.province, ignoreCase = true) &&
                        (it.areaKota ?: "")
                            .trim()
                            .lowercase()
                            .equals(filterArea.city, ignoreCase = true)
            }
        }

        if (filterSize != null){
            result = result.filter { it.size == filterSize.size }
        }

        if(!filterQuery.isNullOrEmpty()){
            result = result
                .filter {
                    (it.komoditas ?: "").lowercase().contains(filterQuery) ||
                            (it.areaKota ?: "").lowercase().contains(filterQuery) ||
                            (it.areaProvinsi ?: "").lowercase().contains(filterQuery)
                }
        }

        // post result filter
        _dataListFilter.postValue(result)
    }

    fun resetFilter() = launch {
        // restore original list item
        _resetListFilter.postValue(mutableListOf<ItemResponseModel>().apply { addAll(tempListFilter) })

        // clear temp item list
        tempListFilter.clear()
    }

    fun orderList(items: List<ItemResponseModel>, order: Bundle) = launch{
        val orderAction = order.getString(OrderItemDialog.ORDER_ACTION) as String
        val option = order.getString(OrderItemDialog.FIELD_OPTION)

        // set temp data items
        if(tempListOrder.isEmpty()){
            tempListOrder.addAll(items)
        }

        if(option != null){
            // preparing result items
            var result: List<ItemResponseModel> = tempListOrder

            // do action ordering
            when(orderAction){
                OrderItemDialog.ORDER_ACTION_ASC -> {
                    result = when(option){
                        OrderItemDialog.ORDER_KOMODITAS -> result.filter { !it.komoditas.isNullOrEmpty() }.sortedBy { it.komoditas }
                        OrderItemDialog.ORDER_PRICE -> result.filter { !it.price.isNullOrEmpty() }.sortedBy { it.price?.toInt() ?: 0 }
                        OrderItemDialog.ORDER_UKURAN -> result.filter { !it.size.isNullOrEmpty() }.sortedBy { it.size?.toInt() ?: 0 }
                        else -> result
                    }
                }

                OrderItemDialog.ORDER_ACTION_DESC -> {
                    result = when(option){
                        OrderItemDialog.ORDER_KOMODITAS -> result.filter { !it.komoditas.isNullOrEmpty() }.sortedByDescending { it.komoditas }
                        OrderItemDialog.ORDER_PRICE -> result.filter { !it.price.isNullOrEmpty() }.sortedByDescending { it.price?.toInt() ?: 0 }
                        OrderItemDialog.ORDER_UKURAN -> result.filter { !it.size.isNullOrEmpty() }.sortedByDescending { it.size?.toInt() ?: 0 }
                        else -> result
                    }
                }
            }

            // post result order
            _dataListOrder.postValue(result)
        }
    }

    fun resetOrder() = launch {
        // restore original list item
        _resetListOrder.postValue(mutableListOf<ItemResponseModel>().apply { addAll(tempListOrder) })

        // clear temp item list
        tempListOrder.clear()
    }

    fun addItemList(data: Bundle) = launch {
        val newItem = useCase.addItem(data)

        _resultAddItem.postValue(newItem)
    }
}