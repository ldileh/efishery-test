package com.efishery.test.di

import android.content.Context
import com.efishery.core.base.BaseService
import com.efishery.test.BuildConfig
import com.efishery.test.config.GlobalConfig
import com.efishery.test.domain.local.LocalDataSource
import com.efishery.test.domain.remote.RemoteDataSource
import com.efishery.test.domain.remote.RemoteService
import com.efishery.test.domain.usecase.MainUseCase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class AppModule {

    @Singleton
    @Provides
    fun provideService() = BaseService.createService(
        serviceClass = RemoteService::class.java,
        url = BuildConfig.SERVER_URL,
        isDebug = GlobalConfig.IS_DEBUG
    )

    @Singleton
    @Provides
    fun provideRemoteDataSource(service: RemoteService) = RemoteDataSource(service)

    /**
     * @param context Context of application
     */
    @Singleton
    @Provides
    fun provideLocalDataSource(
        @ApplicationContext context: Context
    ) = LocalDataSource(context)

    /**
     * @param remoteDataSource Remote data source (example : Endpoint)
     * @param localDataSource local data source (example : database local device or shared preference data)
     */
    @Singleton
    @Provides
    fun provideUseCase(
        remoteDataSource: RemoteDataSource,
        localDataSource: LocalDataSource,
    ) = MainUseCase(remoteDataSource, localDataSource)
}