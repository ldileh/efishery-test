import org.gradle.api.JavaVersion

@Suppress("unused")
object ConfigData {
    const val minSdk = 21
    const val targetSdk = 30
    const val versionCode = 1
    const val versionName = "1.0"
    const val sdk = 30

    const val baseUrlDev = "\"https://stein.efishery.com/v1/storages/5e1edf521073e315924ceab4/\""
    const val baseUrlProd = "\"https://stein.efishery.com/v1/storages/5e1edf521073e315924ceab4/\""

    const val kotlinJvmTarget = "1.8"
}